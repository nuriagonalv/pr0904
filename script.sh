apt-get update
apt-get install apache2 --yes

apt-get install vsftpd --yes
cp /etc/vsftpd.conf /etc/vsftpd.conf_default
systemctl start vsftpd
systemctl enable vsftpd

useradd -m -p '$6$LO6Fd8Y/1vNkhbk.$.Uw30Oa8.Mft737vwanH3CKhQvq.ItVmSan6cVZuyg8x/TeMyysULWC4Vc0lizZxILpQb6N4P.KNYFlgL42Y81' practica
mkdir /home/practica/ftp
chown nobody:nogroup /home/practica/ftp
chmod a-w /home/practica/ftp
mkdir /home/practica/ftp/files
chown practica:practica /home/practica/ftp/files

cp /vagrant/vsftpd.conf /etc

systemctl restart vsftpd
echo "Aprovisionando"
touch /tmp/fichero